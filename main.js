let input1 = document.getElementById('password1');
let input2 = document.getElementById('password2');
input2.setAttribute('type', 'text');

let btn1 = document.getElementById('eye-open');
let btn2 = document.getElementById('eye-close');
let span = document.createElement('span');

let button = document.querySelector('.btn');
button.before(span);

function onButtonClick1 () {
    btn1.classList.toggle('fa-eye');
    btn1.classList.toggle('fa-eye-slash');
    if(btn1.classList.contains('fa-eye-slash')){
        input1.setAttribute('type', 'text');
    }else{
        input1.setAttribute('type', 'password');
    }
}

function onButtonClick2 (){
    btn2.classList.toggle('fa-eye-slash');
    btn2.classList.toggle('fa-eye');
    if(btn2.classList.contains('fa-eye-slash')){
        input2.setAttribute('type', 'text');
    }else{
        input2.setAttribute('type', 'password');
    }
}

function onButtonClick (event){
    event.preventDefault();
    if(`${input1.value}` === `${input2.value}`){
        span.innerHTML = '';
        alert('You are welcome');
    }else{
        span.innerHTML = 'Нужно ввести одинаковые значения';
        span.classList.add('error');
    }
}

button.addEventListener('click', onButtonClick);
btn1.addEventListener('click', onButtonClick1);
btn2.addEventListener('click', onButtonClick2);
